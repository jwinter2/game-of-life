
color dead = color(0,0,0);
color alive = color(255,255,255);


boolean verbose = true; // displays output messages if true
double initPercentAlive = 20;
double totalPercent = 100;
int offset = 5;
int sizeInitial = 10;

int sizeTot;
int numLoops;

void setup() {
  //all three of these numbers should be the same
   sizeTot = 10;
   size(10,10);
   background(dead);
   loop();
 
   println("settings:");
   println("Percent alive at start: " + initPercentAlive/totalPercent * 100);
   println("Offset from top left: " + offset);
   println("initial size of board: " + sizeInitial);
   
   //initialize board
   int numAlive = 0;
   
   loadPixels();
   for(int i = -1 * (sizeInitial/2); i < (sizeInitial/2); i++){
     for (int j = -1 * (sizeInitial/2); j < (sizeInitial/2); j++){ 
       int x = i;
       int y = j * sizeTot;
       
       if(random((int)totalPercent) < initPercentAlive){
         numAlive++;
         pixels[(x + offset) + (y+ (offset * sizeTot))] = alive;
         if(verbose) println("pixel initalised at: x = " + x + " y = " + y/sizeTot );
       }  
     }
     
     
   }
   println("Number alive at start: " + numAlive);
   updatePixels();
}

void draw() {
  numLoops++;
  
  loadPixels();
  
  
  for(int i = sizeInitial ; i < (sizeTot^2); i++){
    if(i % sizeTot != 0 && i % sizeTot != 1 && i > sizeTot && i < (sizeTot^2 - sizeTot - 1)){
      
      
      boolean isAlive;
      if(pixels[i] == alive){
        isAlive = true;
      } else {
        isAlive = false;
      }
      
      int numAlive = 0;
      
      color left = pixels[i - 1];
      color right = pixels[i + 1];
      
      color top = pixels[i - width];
      color topLeft = pixels[i - (width - 1)];
      color topRight = pixels[i - (width + 1)]; 
      
      color bottom = pixels[i + width];    
      color bottomLeft = pixels[i + (width - 1)];
      color bottomRight = pixels[i + (width + 1)]; 
      
      if(left == alive) numAlive++;
      if(right == alive) numAlive++;
      if(top == alive) numAlive++;
      if(topLeft == alive) numAlive++;
      if(topRight == alive) numAlive++;
      if(bottom == alive) numAlive++;
      if(bottomLeft == alive) numAlive++;
      if(bottomRight == alive) numAlive++;
      
      if(numAlive > 0 && verbose){
        println("number of alive around current pixel " + numAlive);
      }
      
      
      //Any live cell with fewer than two live neighbours dies, as if by underpopulation.
      if(isAlive == true && numAlive < 2){
        pixels[i] = dead;
        
        if(verbose) println("pixel killed");
        
      //Any live cell with two or three live neighbours lives on to the next generation.
      }else if(isAlive == true && (numAlive == 3 || numAlive == 2)){      
        pixels[i] = alive;        
        if(verbose) println("pixel revived");
        
      //Any live cell with more than three live neighbours dies, as if by overpopulation.
      }else if(isAlive == true && numAlive > 3){     
        pixels[i] = dead;
        
        if(verbose) println("pixel killed");
        
      //Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
      }else if(isAlive == false && numAlive == 3){     
        pixels[i] = alive;        
        if(verbose) println("pixel revived");
      }
      
    }
    
  }
  if(verbose) println("update " + numLoops);
  updatePixels();
}
